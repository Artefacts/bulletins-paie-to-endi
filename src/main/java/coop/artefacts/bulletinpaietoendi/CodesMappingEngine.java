/*
 */
package coop.artefacts.bulletinpaietoendi;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 * https://www.baeldung.com/apache-commons-csv
 */
public class CodesMappingEngine {

    public class AnalyticsMappingResult {

        public int missingMatricule = 0;
        public int missingAnalytics = 0;
    }

    public AnalyticsMappingResult processFile(File file, DefaultTableModel model) throws IOException {

        AnalyticsMappingResult result = new AnalyticsMappingResult();

        model.setRowCount(0);

        Reader in = new FileReader(file);
        CSVFormat csvFormat = CSVFormat.DEFAULT.builder()
                .setSkipHeaderRecord(false)
                .setDelimiter(";")
                .build();

        Iterable<CSVRecord> records = csvFormat.parse(in);

        /**
         * "Nom" "Prénom" "Compte_analytique 1" "Compte_analytique 2"
         * "Identifiant interne"
         */
        Map<String, Integer> headers = new HashMap<>();

        boolean firstRow = true;
        for (CSVRecord record : records) {

            if (firstRow) {
                firstRow = false;
                for (int i = 0; i < record.size(); i++) {
                    headers.put(record.get(i), i);
                }
            } else {
                String[] row = new String[4];
                row[0] = record.get(headers.get("Nom"));
                row[1] = record.get(headers.get("Prénom"));
                row[2] = record.get(headers.get("Identifiant interne"));
                if (row[2].equals("")) {
                    result.missingMatricule++;
                }
                row[3] = record.get(headers.get("Compte_analytique 2"));
                if (row[3].equals("")) {
                    result.missingAnalytics++;
                }
                model.addRow(row);
            }
        }
        return result;
    }

    static final String PREF_PREFIX = "code_";

    String[] columns = new String[]{"lastname", "firstname", "matricule", "code_analytic"};

    public AnalyticsMappingResult loadFromPrefs(Preferences prefs, DefaultTableModel model) {
        AnalyticsMappingResult result = new AnalyticsMappingResult();

        model.setRowCount(0);

        boolean found = true;
        int rows = 0;
        String val;
        do {
            rows++;
            String[] row = new String[4];
            int cols = 0;
            for (String col : columns) {
                val = prefs.get(PREF_PREFIX + "." + rows + "." + col, null);
                if (val == null) {
                    found = false;
                    break;
                }
                row[cols++] = val;
            }
            if (found) {
                if (row[2].equals("")) {
                    result.missingMatricule++;
                }
                if (row[3].equals("")) {
                    result.missingAnalytics++;
                }

                model.addRow(row);
            }
        } while (found);

        return result;
    }

    public void saveToPrefs(Preferences prefs, DefaultTableModel model) {
        String val;
        for (int r = 0; r < model.getRowCount(); r++) {
            for (int c = 0; c < columns.length; c++) {
                val = (String) model.getValueAt(r, c);
                prefs.put(PREF_PREFIX + "." + (r + 1) + "." + columns[c], val);

            }
        }
    }

}
