/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package coop.artefacts.bulletinpaietoendi.essais;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;

/**
 * https://tutoriels.edu.lat/pub/pdfbox/pdfbox-merging-multiple-pdf-documents/pdfbox-fusion-de-plusieurs-documents-pdf
 */
public class MergePdfs {

    public static void main(String args[]) {

        PDFMergerUtility PDFmerger = new PDFMergerUtility();
        PDFmerger.setDestinationFileName("/home/cyrille/Taf/Artefacts/SI/enDI/bulletins-salaires/merged.pdf");

        File file;
        try {
            file = new File("/home/cyrille/Taf/Artefacts/Administratif/Paie/2022/01-GIQUELLO_CYRILLE_01_2022_1.pdf");
            PDFmerger.addSource(file);
            file = new File("/home/cyrille/Taf/Artefacts/Administratif/Paie/2022/02-GIQUELLO_CYRILLE_02_2022_1.pdf");
            PDFmerger.addSource(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MergePdfs.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            PDFmerger.mergeDocuments(MemoryUsageSetting.setupMixed(1024 + 1000 * 500));
        } catch (IOException ex) {
            Logger.getLogger(MergePdfs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
