/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package coop.artefacts.bulletinpaietoendi.essais;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

/**
 *
 * @author cyrille
 */
public class ProcessPdfPayslips {

    static class UnreadableBulletin extends Exception {

        public UnreadableBulletin(String message, Throwable cause) {
            super(message, cause);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        String folder = "/home/cyrille/Taf/Artefacts/SI/enDI/bulletins-salaires/01/Bulletins et Avis de Paiements";
        int count = 0;

        for (File file : new File(folder).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".pdf");
            }
        })) {
            count++;

            /* DEBUG
            if (!file.getName().equals("GIQUELLO_CYRILLE_01_2022_1.pdf")) {
                continue;
            }
            if (count >= 1) {
                break;
            }
             */
            try {
                System.out.println("File: " + file.getName());
                PDDocument document = PDDocument.load(file);

                searchForText(document, 1, "Matricule");

                document.close();

            } catch (IOException ex) {
                Logger.getLogger(ProcessPdfPayslips.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnreadableBulletin ex) {
                Logger.getLogger(ProcessPdfPayslips.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void searchForText(PDDocument document, int page, String searchTerm)
            throws IOException, UnreadableBulletin {

        class RowCounter {

            int count = 0;

            @Override
            public String toString() {
                return Integer.toString(this.count);
            }
        }
        RowCounter linesCount = new RowCounter();

        class StripResult {

            public String original_line;
            public String matricule;
            public String firstname;
            public String lastname;
            public int year;
            public int month;

            @Override
            public String toString() {
                return "Bulletin du mois " + month + " année " + year + " pour " + firstname + " " + lastname + " (matricule:" + matricule + ")";
            }
        }
        StripResult result = new StripResult();

        PDFTextStripper stripper = new PDFTextStripper() {
            @Override
            protected void writeString(String text, List<TextPosition> textPositions) throws IOException {

                linesCount.count++;

                if (linesCount.count == 1) {

                    System.out.println("" + linesCount + ": " + text);
                    // La 1ère ligne de l'extraction du texte doit être de la forme:
                    // 10168##BULLETIN##01-2022##00045##Giquello##Cyrille##52784052400021 
                    result.original_line = text;
                    String[] parts = text.split("##");
                    //System.out.println(Arrays.toString(parts));
                    result.matricule = parts[3];
                    result.lastname = parts[4];
                    result.firstname = parts[5];
                    parts = parts[2].split("-");
                    result.month = Integer.parseInt(parts[0]);
                    result.year = Integer.parseInt(parts[1]);
                }
            }
        };
        stripper.setSortByPosition(true);
        stripper.setStartPage(page);
        stripper.setEndPage(page);
        try {
            stripper.getText(document);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new UnreadableBulletin("Unknow format for line: "+result.original_line, ex);
        }

        System.out.println("Found: " + result);
    }
}
