/*
 */
package coop.artefacts.bulletinpaietoendi;

import coop.artefacts.bulletinpaietoendi.essais.ProcessPdfPayslips;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

/**
 *
 * @author cyrille
 */
public class BulletinsPaieEngine {

    public class ProcessResult {

        public int bulletinsCount = 0;
        public int ignoredCount = 0;
        public int errorCount = 0;
        public ArrayList<FileResult> files;
    }

    public enum FileStatus {
        OK,
        IGNORED,
        DIFFRENT_DATE;
    }

    public class FileResult {

        String filename;
        FileStatus status;
        public String original_line;
        public String matricule;
        public String firstname;
        public String lastname;
        public int year;
        public int month;

        @Override
        public String toString() {
            return "Bulletin du mois " + month + " année " + year + " pour " + firstname + " " + lastname + " (matricule:" + matricule + ")";
        }
    }

    static class UnreadableBulletin extends Exception {

        public UnreadableBulletin(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public ProcessResult processFolder(File[] files) {
        ProcessResult result = new ProcessResult();
        result.files = new ArrayList<>();
        int year = 0, month = 0;

        for (File file : files) {
            try {
                System.out.println("File: " + file.getName());
                PDDocument document = PDDocument.load(file);

                FileResult fileResult = new FileResult();
                result.files.add(fileResult);

                fileResult.filename = file.getName();

                try {
                    searchForBulletinData(document, 1, fileResult);
                } catch (UnreadableBulletin ex) {
                    Logger.getLogger(ProcessPdfPayslips.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (fileResult.matricule != null) {
                    /**
                     * 1er fichier c'est OK
                     */
                    if (year == 0 && month == 0) {
                        year = fileResult.year;
                        month = fileResult.month;
                        fileResult.status = FileStatus.OK;
                        result.bulletinsCount++;
                    } else {
                        /**
                         * fichiers suivant doivent avoir la même date
                         */
                        if (year == fileResult.year && month == fileResult.month) {
                            fileResult.status = FileStatus.OK;
                            result.bulletinsCount++;
                        } else {
                            fileResult.status = FileStatus.DIFFRENT_DATE;
                            result.errorCount++;
                        }
                    }
                } else {
                    fileResult.status = FileStatus.IGNORED;
                    result.ignoredCount++;
                }

                document.close();

            } catch (IOException ex) {
                Logger.getLogger(ProcessPdfPayslips.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    public static void searchForBulletinData(PDDocument document, int page, FileResult fileResult)
            throws IOException, UnreadableBulletin {

        class RowCounter {

            int count = 0;

            @Override
            public String toString() {
                return Integer.toString(this.count);
            }
        }
        RowCounter linesCount = new RowCounter();

        PDFTextStripper stripper = new PDFTextStripper() {
            @Override
            protected void writeString(String text, List<TextPosition> textPositions) throws IOException {

                linesCount.count++;

                if (linesCount.count != 1) {
                    return;
                }

                System.out.println("" + linesCount + ": " + text);
                // La 1ère ligne de l'extraction du texte doit être de la forme:
                // 10168##BULLETIN##01-2022##00045##Giquello##Cyrille##52784052400021 
                fileResult.original_line = text;
                String[] parts = text.split("##");
                //System.out.println(Arrays.toString(parts));
                fileResult.matricule = parts[3];
                fileResult.lastname = parts[4];
                fileResult.firstname = parts[5];
                parts = parts[2].split("-");
                fileResult.month = Integer.parseInt(parts[0]);
                fileResult.year = Integer.parseInt(parts[1]);

            }
        };
        stripper.setSortByPosition(true);
        stripper.setStartPage(page);
        stripper.setEndPage(page);
        try {
            stripper.getText(document);
        } catch (ArrayIndexOutOfBoundsException ex) {
            throw new UnreadableBulletin("Unknow format for line: " + fileResult.original_line, ex);
        }

        System.out.println("Found: " + fileResult);
    }
}
