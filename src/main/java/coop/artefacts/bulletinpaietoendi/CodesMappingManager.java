/*
 */
package coop.artefacts.bulletinpaietoendi;

/**
 *
 * @author cyrille
 */
public interface CodesMappingManager {
    public void setCodesValid( boolean valid);
    public String getAnalyticForMatricule( String matricule );
}
