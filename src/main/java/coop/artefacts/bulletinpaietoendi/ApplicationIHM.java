/*
 */
package coop.artefacts.bulletinpaietoendi;

import javax.swing.DefaultListModel;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;

/**
 * JList: https://docs.oracle.com/javase/tutorial/uiswing/components/list.html
 *
 * @author cyrille
 */
public class ApplicationIHM extends javax.swing.JFrame implements CodesMappingManager {


    /**
     * Creates new form NewJFrame
     */
    public ApplicationIHM() {

        initComponents();
        analyticsMappingIHM1.loadPrefs();
    }

    protected DefaultListModel<String> listModel = new DefaultListModel<String>();

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        bulletinsIHM1 = new coop.artefacts.bulletinpaietoendi.BulletinsIHM();
        analyticsMappingIHM1 = new coop.artefacts.bulletinpaietoendi.CodesMappingIHM();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bulletins de paie vers enDI");
        setMinimumSize(new java.awt.Dimension(698, 495));

        jTabbedPane1.addTab("Bulletins", bulletinsIHM1);
        jTabbedPane1.addTab("Codes analytiques", analyticsMappingIHM1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Bulletins");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    HTMLEditorKit getHTMLEditorKit() {
        HTMLEditorKit htmlKit = new HTMLEditorKit();
        StyleSheet css = htmlKit.getStyleSheet();
        /*if (css.getStyleSheets() == null) {
            css = new StyleSheet();
        }*/
        css.addRule("p {margin: .2rem;}");
        css.addRule("span.ok {color: #0F0;}");
        //htmlKit.setStyleSheet(css);
        return htmlKit;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ApplicationIHM.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ApplicationIHM.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ApplicationIHM.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ApplicationIHM.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ApplicationIHM().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private coop.artefacts.bulletinpaietoendi.CodesMappingIHM analyticsMappingIHM1;
    private coop.artefacts.bulletinpaietoendi.BulletinsIHM bulletinsIHM1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void setCodesValid(boolean valid) {
        bulletinsIHM1.setCodesValid(valid);
    }

    @Override
    public String getAnalyticForMatricule(String matricule) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
