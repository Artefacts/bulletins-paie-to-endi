
## Bulletin Pdf

La 1ere ligne du bulletin contient les données nécessaires :-)

```
--- exec:3.1.0:exec (default-cli) @ mavenproject1 ---
File: GIQUELLO_CYRILLE_01_2022_1.pdf
1: 10168##BULLETIN##01-2022##00045##Giquello##Cyrille##52784052400021 
2: ARTEFACTS
3: BULLETIN DE SALAIRE
4: 10168
5: 30 rue André Theuriet
```
## App design

icons: https://www.iconfinder.com/iconsets/ui-essential-17

## User Preferences

https://docs.oracle.com/en/java/javase/17/docs/api/java.prefs/java/util/prefs/Preferences.html

Preferences.MAX_KEY_LENGTH : Maximum length of string allowed as a key (80 characters).
Preferences.MAX_VALUE_LENGTH : Maximum length of string allowed as a value (8192 characters).
